use serde_derive::{Deserialize, Serialize};

/// Tags allow users to collect individual entries.
/// The format for a tag id is `user/:userId/tag/:label`.
/// The laber of a tag is initally set to the last part of the id.
/// The label cannot contain any of the following characters `"`, `<`, `>`, `?`, `&`, `/`, `\`, `^`
#[derive(Debug, Serialize, Deserialize)]
pub struct Tag {
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
}

impl Tag {
    pub fn decompose(self) -> (String, Option<String>, Option<String>) {
        (self.id, self.label, self.description)
    }
}
