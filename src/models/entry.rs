use super::Category;
use super::Tag;
use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Entry {
    pub id: String,
    #[serde(default)]
    pub title: Option<String>,
    #[serde(default)]
    pub content: Option<Content>,
    #[serde(default)]
    pub summary: Option<Content>,
    #[serde(default)]
    pub author: Option<String>,
    pub crawled: u64,
    #[serde(default)]
    pub recrawled: Option<u64>,
    pub published: u64,
    #[serde(default)]
    pub updated: u64,
    #[serde(default)]
    pub alternate: Option<Vec<Link>>,
    #[serde(default)]
    pub origin: Option<Origin>,
    #[serde(default)]
    pub keywords: Option<Vec<String>>,
    #[serde(default)]
    pub visual: Option<Visual>,
    pub unread: bool,
    #[serde(default)]
    pub tags: Option<Vec<Tag>>,
    #[serde(default)]
    pub categories: Option<Vec<Category>>,
    #[serde(default)]
    pub engagement: Option<i32>,
    #[serde(default)]
    #[serde(rename = "actionTimestamp")]
    pub action_timestamp: Option<i32>,
    #[serde(default)]
    pub enclosure: Option<Vec<Link>>,
    pub fingerprint: String,
    #[serde(rename = "originId")]
    pub origin_id: String,
    #[serde(default)]
    pub sid: Option<String>,
}

impl Entry {
    pub fn decompose(
        self,
    ) -> (
        String,
        Option<String>,
        Option<Content>,
        Option<Content>,
        Option<String>,
        u64,
        Option<u64>,
        u64,
        u64,
        Option<Vec<Link>>,
        Option<Origin>,
        Option<Vec<String>>,
        Option<Visual>,
        bool,
        Option<Vec<Tag>>,
        Option<Vec<Category>>,
        Option<i32>,
        Option<i32>,
        Option<Vec<Link>>,
        String,
        String,
        Option<String>,
    ) {
        (
            self.id,
            self.title,
            self.content,
            self.summary,
            self.author,
            self.crawled,
            self.recrawled,
            self.published,
            self.updated,
            self.alternate,
            self.origin,
            self.keywords,
            self.visual,
            self.unread,
            self.tags,
            self.categories,
            self.engagement,
            self.action_timestamp,
            self.enclosure,
            self.fingerprint,
            self.origin_id,
            self.sid,
        )
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Content {
    pub content: String,
    #[serde(default)]
    pub direction: Option<String>,
}

impl Content {
    pub fn decompose(self) -> (String, Option<String>) {
        (self.content, self.direction)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Link {
    pub href: String,
    #[serde(default)]
    #[serde(rename = "type")]
    pub _type: Option<String>,
}

impl Link {
    pub fn decompose(self) -> (String, Option<String>) {
        (self.href, self._type)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Origin {
    #[serde(rename = "streamId")]
    pub stream_id: String,
    pub title: String,
    #[serde(rename = "htmlUrl")]
    pub html_url: String,
}

impl Origin {
    pub fn decompose(self) -> (String, String, String) {
        (self.stream_id, self.title, self.html_url)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Visual {
    pub url: String,
    #[serde(default)]
    pub width: Option<u32>,
    #[serde(default)]
    pub height: Option<u32>,
    #[serde(default)]
    #[serde(rename = "contentType")]
    pub content_type: Option<String>,
}

impl Visual {
    pub fn decompose(self) -> (String, Option<u32>, Option<u32>, Option<String>) {
        (self.url, self.width, self.height, self.content_type)
    }
}
