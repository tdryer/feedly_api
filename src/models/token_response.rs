use super::super::AccessToken;
use super::super::RefreshToken;
use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct AccessTokenResponse {
    /// The feedly user id
    pub id: String,

    /// A token that may be used to obtain a new access token.
    /// Refresh tokens are valid until the user revokes access.
    pub refresh_token: RefreshToken,

    /// A token that may be used to access APIs.
    /// Access tokens have an expiration (see `expires_in`).
    pub access_token: AccessToken,

    /// The remaining lifetime of the `access_token` in seconds.
    pub expires_in: u32,

    /// Indicates the type of token returned.
    /// At this time, this field will always have the value of `Bearer`
    pub token_type: String,

    /// Indicates the user plan (`standard`, `pro` or `business`).
    pub plan: String,

    pub provider: String,

    /// (optional) The state that was passed in. In this case `feedly-api rust crate`.
    #[serde(default)]
    pub state: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RefreshTokenResponse {
    /// The feedly user id.
    pub id: String,

    /// The new access token.
    pub access_token: AccessToken,

    /// The remaining lifetime of the access token in seconds.
    pub expires_in: u32,

    /// Indicates the type of token returned.
    /// At this time, this field will always have the value of `Bearer`
    pub token_type: String,

    /// Indicates the user plan (`standard`, `pro` or `business`).
    pub plan: String,

    pub provider: String,

    /// `https://cloud.feedly.com/subscriptions`
    pub scope: String,
}
