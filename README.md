# feedly_api

[![Crates.io Status](https://img.shields.io/crates/v/feed-rs.svg)](https://crates.io/crates/feedly_api)

Rust implementation of the [feedly API](https://developer.feedly.com/).


## Usage

Add the dependency to your `Cargo.toml`.

```toml
[dependencies]
feedly_api = "0.2"
```

## Getting up and running

### Generate the URL to the login page

The client id and secret are issued by feedly for each client on formal request (can also be denied by feedly).
```rust
let login_url = FeedlyApi::login_url("client_id", "client_secret").unwrap();
let redirect_base_url = FeedlyApi::redirect_uri().unwrap();
```

The `login_url` will redirect to a URL with the base of `redirect_base_url`. This URL contains the `AuthCode` and is needed for the next step.

### Parsing the redirected URL after successful login

```rust
let auth_code = FeedlyApi::parse_redirected_url(redirect_url).unwrap();
```

### Getting the `access_token`

With the `AuthCode` form the previous step the `access_token` and friends can finally be requested.

```rust
let client = reqwest::Client::new();
let token_response = FeedlyApi::request_auth_token("client_id", "client_secret", auth_code, &client).unwrap();
```


### Creating a FeedlyApi instance

```rust
let access_token_expires = FeedlyApi::parse_expiration_date(&token_response.expires_in.to_string()).unwrap();
let feedly_api = FeedlyApi::new(
    "client_id",
    "client_secret",
    token_response.access_token,
    token_response.refresh_token,
    access_token_expires).unwrap();
```
